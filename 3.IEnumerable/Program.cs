﻿using System.Collections;

IEnumerable enumerable = new Enumerable();

foreach (object item in enumerable) // hãy đặt break point ở đây và quan sát
{
    Console.WriteLine(item);
}

class Enumerable : IEnumerable
{
    public IEnumerator GetEnumerator()
    {
        int[] arr = new int[] { 1, 2, 3, 4 };
        IEnumerator enumerator = new Enumerator(arr);
        return enumerator;
    }
}

class Enumerator : IEnumerator
{
    private int[] data;
    private int position = -1;
    public Enumerator(int[] _data)
    {
        data = _data;
    }

    public object Current
    {
        get
        {
            if (position < 0 || position >= data.Length)
                throw new InvalidOperationException();
            return data[position] * 2;
        }
    }

    public bool MoveNext()
    {
        position++;
        return (position < data.Length);
    }

    public void Reset()
    {
        position = -1;
    }
}
