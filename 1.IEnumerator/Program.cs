﻿using System.Collections;

int[] collection = new int[] { 1, 3, 5, 7 };
IEnumerator enumerator = new Enumerator(collection);

while (enumerator.MoveNext())
{
    Console.WriteLine(enumerator.Current);
}


/// <summary>
/// Triển khai IEnumerator 
/// </summary>
class Enumerator : IEnumerator
{
    private int[] _items;
    private int position = -1;
    public Enumerator(int[] items) => _items = items;
    public object Current
    {
        get
        {
            if (position < 0 || position >= _items.Length)
                throw new InvalidOperationException();
            return _items[position] * 3; // kiểu số nguyên có thể ngầm định chuyển sang kiểu object
        }
    }

    public bool MoveNext()
    {
        position++;
        return position < _items.Length;
    }

    public void Reset() => position = -1;
}