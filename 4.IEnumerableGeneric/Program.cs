using System.Collections;

IEnumerable<int> enumerable = new Enumerable<int>(new int[] { 1, 2, 3, 4 });

foreach (int item in enumerable) // hãy đặt break point ở đây và quan sát
{
    Console.WriteLine(item);
}

class Enumerable<T> : IEnumerable<T>
{
    private readonly T[] _items;

    public Enumerable(T[] items)
    {
        _items = items;
    }

    public IEnumerator<T> GetEnumerator() => new Enumerator<T>(_items);

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}

class Enumerator<T> : IEnumerator<T>
{
    private T[] _items;
    private int position = -1;
    public Enumerator(T[] items) => _items = items;
    public T Current
    {
        get
        {
            if (position < 0 || position >= _items.Length)
                throw new InvalidOperationException();
            return _items[position];
        }
    }

    public bool MoveNext()
    {
        position++;
        return position < _items.Length;
    }

    public void Reset() => position = -1;

    object IEnumerator.Current => Current;
    public void Dispose() { }
}
