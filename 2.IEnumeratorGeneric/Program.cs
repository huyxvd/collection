﻿using System.Collections;

int[] arrayInt = new int[] { 1, 3, 5, 7 };
IEnumerator<int> enumerator = new Enumerator<int>(arrayInt);

while (enumerator.MoveNext())
{
    Console.WriteLine(enumerator.Current);
}

// thử với string

string[] arrayString = new string[] { "apple", "banana", "orange" };
IEnumerator<string> enumeratorString = new Enumerator<string>(arrayString);

while (enumeratorString.MoveNext())
{
    Console.WriteLine(enumeratorString.Current);
}

class Enumerator<T> : IEnumerator<T>
{
    private T[] data;
    private int position = -1;
    public Enumerator(T[] _data) => data = _data;
    public T Current
    {
        get
        {
            if (position < 0 || position >= data.Length)
                throw new InvalidOperationException();
            return data[position];
        }
    }

    public bool MoveNext()
    {
        position++;
        return position < data.Length;
    }

    public void Reset() => position = -1;

    object IEnumerator.Current
        => throw new NotImplementedException();
    public void Dispose()
        => throw new NotImplementedException();
}